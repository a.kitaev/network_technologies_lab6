package portforwarder

import java.net.InetAddress

fun main(args: Array<String>) {
    if (args.size != 3) {
        println("Example call: <lport> <rhost> <rport>")
        return
    }

    val (lportStr, rhostStr, rportStr) = args

    val arguments: Arguments
    try {
        arguments = Arguments(
            lportStr.toInt(),
            InetAddress.getByName(rhostStr),
            rportStr.toInt()
        )
        checkArguments(arguments)
    } catch (e: Exception) {
        e.localizedMessage
        return
    }

    val portForwarder = PortForwarder(arguments)
    println("Create port forwarder")
    try {
        portForwarder.run()
        println("Port forwarder run")
    } catch (e: Exception) {
        e.localizedMessage
        return
    }

    Runtime.getRuntime().addShutdownHook(Thread(portForwarder::stop))
}