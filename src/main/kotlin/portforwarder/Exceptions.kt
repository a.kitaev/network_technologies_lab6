package portforwarder

class ArgumentsException(msg: String) : Exception("Argument exception: $msg")