package portforwarder

import java.net.InetSocketAddress
import java.nio.channels.SelectionKey
import java.nio.channels.Selector
import java.nio.channels.ServerSocketChannel
import java.nio.channels.SocketChannel

class PortForwarder(private val arguments: Arguments) {

    private var isInterrupt = false

    fun run() {
        val selector = getSelector()
        while (!isInterrupt) {
            if (selector.select() == 0) {
                continue
            }

            resolve(selector)
        }
    }

    private fun resolve(selector: Selector) {
        val iter = selector.selectedKeys().iterator()
        while (iter.hasNext()) {
            val key = iter.next()

            try {

                if (!key.isValid) {
                    println("Key not valid ${key.channel()}")
                    key.cancel()
                    (key.attachment() as Proxy).wantClose()
                    continue
                }

                if (key.isConnectable) {
                    handleConnect(key)
                }

                if (key.isAcceptable && !handleAccept(key, selector)) {
                    continue
                }

                if (key.isReadable) {
                    (key.attachment() as Proxy).handleRead()
                }

                if (key.isWritable) {
                    (key.attachment() as Proxy).handleWrite()
                }

                if (key.attachment() is Proxy) {
                    val attachment = key.attachment() as Proxy
                    if (attachment.isReadyToClose()) {
                        attachment.close()
                        key.cancel()
                    }
                }
            } catch (e: Exception) {
                key.cancel()
                (key.attachment() as Proxy).wantClose()
            }

            iter.remove()
        }
    }

    fun stop() {
        isInterrupt = true
    }

    private fun handleAccept(key: SelectionKey, selector: Selector): Boolean {
        val localServerChannel = key.channel() as ServerSocketChannel
        val clientChannel = localServerChannel.accept()
        println("Received connection from ${clientChannel.remoteAddress}")

        val remoteServer: Proxy
        val client: Proxy

        try {
            println("Try connect to ${arguments.rhost}:${arguments.rport}")
            val remoteServerChannel = SocketChannel.open().apply {
                this.configureBlocking(false)
                this.connect(InetSocketAddress(arguments.rhost, arguments.rport))
            }

            remoteServer = Proxy(remoteServerChannel, selector)
            client = Proxy(clientChannel, selector)
            remoteServerChannel.register(selector, SelectionKey.OP_CONNECT, remoteServer)

        } catch (e: Exception) {
            clientChannel.close()
            key.cancel()
            return false
        }

        remoteServer.pair = client
        client.pair = remoteServer

        clientChannel.configureBlocking(false)
        clientChannel.register(selector, 0, clientChannel)
        return true
    }

    private fun handleConnect(key: SelectionKey) {
        val channel = key.channel() as SocketChannel
        val attachment = key.attachment() as Proxy

        if (channel.finishConnect()) {
            println("First connect")
            attachment.registerRead()
            attachment.pair?.registerRead()
        } else {
            println("Can't connect")
            attachment.wantClose()
        }
    }

    private fun getSelector(): Selector {
        val selector = Selector.open()
        ServerSocketChannel.open().apply {
            this.bind(InetSocketAddress(arguments.lport))
            this.configureBlocking(false)
            this.register(selector, SelectionKey.OP_ACCEPT)
            println("Listen to 0.0.0.0:${arguments.lport}")
        }
        return selector
    }
}