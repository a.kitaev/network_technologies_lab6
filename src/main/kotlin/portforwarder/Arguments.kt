package portforwarder

import java.net.InetAddress

data class Arguments(
    var lport: Int,
    var rhost: InetAddress,
    var rport: Int
)

private fun checkPort(arg: Int, msg: String) {
    val range = 1..65535
    if (arg !in range) {
        throw ArgumentsException(msg)
    }
}

fun checkArguments(args: Arguments) {
    checkPort(args.lport, "lport")
    checkPort(args.rport, "rport")
}