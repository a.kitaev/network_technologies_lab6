package portforwarder

import java.nio.ByteBuffer
import java.nio.channels.SelectionKey
import java.nio.channels.Selector
import java.nio.channels.SocketChannel

class Proxy(
    private val channel: SocketChannel,
    private val selector: Selector
) {

    var pair: Proxy? = null
    private var buffer = ByteBuffer.allocate(BUFFER_SIZE)
    private var wantShutdownOutput = false
    private var wantClose = false
    private var interestOps = 0

    fun handleRead() {
        val read = channel.read(pair?.buffer)
        if (read == END_OF_STREAM) {
            unregisterRead()
            channel.shutdownInput()
            tryRequestClose()
            pair?.wantShutdownOutput = true
        }

        if (!pair!!.buffer.hasRemaining()) {
            unregisterRead()
        }

        pair?.registerWrite()
        println("Read $read bytes from ${channel.remoteAddress}")
    }

    fun handleWrite() {
        buffer.flip()
        val sent = channel.write(buffer)
        println("Sent $sent bytes to ${channel.remoteAddress}")
        buffer.compact()

        if (buffer.hasRemaining()) {
            val isShutdown = pair?.channel?.socket()?.isInputShutdown
            if (null != isShutdown && !isShutdown) {
                pair?.registerRead()
            }
        }

        if (buffer.position() == 0) {
            if (wantShutdownOutput) {
                channel.shutdownOutput()
                tryRequestClose()
            }
            unregisterWrite()
        }
    }

    fun wantClose() {
        wantClose = true
        pair?.wantClose = true
    }

    fun isReadyToClose(): Boolean {
        val pairWantClose = pair?.wantClose ?: true
        val pairBufferPos = pair?.buffer?.position() ?: 0

        return wantClose && pairWantClose && (buffer.position() == 0) && (pairBufferPos == 0)
    }

    fun close() {
        channel.close()
        pair?.channel?.close()
    }

    fun registerRead() {
        interestOps = interestOps or SelectionKey.OP_READ
        channel.register(selector, interestOps, this)
    }

    private fun tryRequestClose() {
        if (interestOps == 0) {
            wantClose = true
            pair?.wantClose = true
        }
    }

    private fun unregisterRead() {
        interestOps = interestOps and SelectionKey.OP_READ.inv()
        channel.register(selector, interestOps, this)
    }

    private fun registerWrite() {
        interestOps = interestOps or SelectionKey.OP_WRITE
        channel.register(selector, interestOps, this)
    }

    private fun unregisterWrite() {
        interestOps = interestOps and SelectionKey.OP_WRITE.inv()
        channel.register(selector, interestOps, this)
    }

    companion object {
        const val BUFFER_SIZE = 1024 * 10
        const val END_OF_STREAM = -1
    }
}